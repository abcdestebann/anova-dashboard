import React, { Component } from 'react';

// THEME
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import configTheme from './config/theme';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import MomentUtils from '@date-io/moment';

// ROUTER
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom'

// AUTH 
import VerifyAuthentication from './authentication'

// COMPONENTS
import Login from './pages/Login';
import CreateNewTemplate from './pages/CreateNewTemplate';
import Dashboard from './components/Layout';

// REDUX
import { Provider } from 'react-redux';
import store from './store';

const theme = createMuiTheme(configTheme);

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <MuiPickersUtilsProvider utils={MomentUtils}>
          <MuiThemeProvider theme={theme}>
            <Router>
              <Switch>
                <Route exact path="/login" component={Login} />
                <Route exact path="/create-new-template" component={CreateNewTemplate} />
                <VerifyAuthentication path="/" component={Dashboard} />
              </Switch>
            </Router>
          </MuiThemeProvider>
        </MuiPickersUtilsProvider>
      </Provider>
    );
  }
}

export default App;