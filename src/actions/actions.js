import { firestore as db } from '../config/firebase'
import { SET_DATA_LIST_ACTIONS } from './actiontypes'
import { ACTIONS_COLLECTION } from '../config/collections'

export function getActions() {
  return async (dispatch) => {
    try {
      let actions = []

      const result = await db.collection(ACTIONS_COLLECTION).get()

      result.forEach(doc => {
        actions = [...actions, { id: doc.id, ...doc.data() }]
      });

      dispatch(setDataListActions(actions))
    } catch (error) {
      console.log('get schedules', error)
    }
  }
}

const setDataListActions = (payload) => ({
  type: SET_DATA_LIST_ACTIONS,
  payload
});