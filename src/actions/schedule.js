import { firestore as db } from '../config/firebase'
import { SET_DATA_LIST_SCHEDULES } from './actiontypes'
import { SCHEDULES_COLLECTION, LOGS_COLLECTION } from '../config/collections'


export function getSchedules() {
  return async (dispatch) => {
    try {
      let schedules = []

      const result = await db.collection(SCHEDULES_COLLECTION).get()

      result.forEach(doc => {
        schedules = [...schedules, { id: doc.id, ...doc.data() }]
      });

      dispatch(setDataListSchedules(schedules))
    } catch (error) {
      console.log('get schedules', error)
    }
  }
}

export function saveSchedule(schedule) {
  return async (dispatch, getState) => {
    try {
      const { user } = getState().userReducer
      const { schedules } = getState().scheduleReducer

      const result = await db.collection(SCHEDULES_COLLECTION).add(schedule)
      schedule.id = result.id
      await db.collection(SCHEDULES_COLLECTION).doc(result.id).set(schedule)

      const newSchedules = [...schedules, schedule]

      dispatch(setDataListSchedules(newSchedules))

      const log = createLog('create', user)

      await db.collection(SCHEDULES_COLLECTION).doc(result.id).collection(LOGS_COLLECTION).doc().set(log)
    } catch (error) {
      console.log('save schedule', error)
    }
  }
}
export function updateSchedule(scheduleId, data) {
  return async (dispatch, getState) => {
    try {
      const { user } = getState().userReducer
      const { schedules } = getState().scheduleReducer

      await db.collection(SCHEDULES_COLLECTION).doc(scheduleId).set(data)

      const index = schedules.map(schedule => schedule.id).indexOf(scheduleId)
      if (index != -1) {
        const newSchedules = [...schedules]
        newSchedules[index] = data
        
        dispatch(setDataListSchedules(newSchedules))
  
        const log = createLog('update', user)
        await db.collection(SCHEDULES_COLLECTION).doc(scheduleId).collection(LOGS_COLLECTION).doc().set(log)
      }
    } catch (error) {
      console.log('update schedule', error)
    }
  }
}

export function deleteSchedule(scheduleId) {
  return async (dispatch, getState) => {
    try {
      const { user } = getState().userReducer
      const { schedules } = getState().scheduleReducer
      const newSchedules = schedules.filter(item => item.id !== scheduleId)

      dispatch(setDataListSchedules(newSchedules))

      await db.collection(SCHEDULES_COLLECTION).doc(scheduleId).update({ isActived: false })

      const log = createLog('delete', user)

      await db.collection(SCHEDULES_COLLECTION).doc(scheduleId).collection(LOGS_COLLECTION).doc().set(log)
    } catch (error) {
      console.log('delete schedule', error);
    }
  }
}

function createLog(action, user) {
  return {
    action,
    date_log: new Date(),
    user_id: user.id
  }
}


const setDataListSchedules = (payload) => ({
  type: SET_DATA_LIST_SCHEDULES,
  payload
});