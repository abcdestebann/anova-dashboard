import { firestore as db } from '../config/firebase'
import { SET_DATA_LIST_TEMPLATES, SET_DATA_TEMPLATE_TO_EDIT } from './actiontypes'
import { TEMPLATES_COLLECTION, TEMPLATES_PAGES_COLLECTION, TEMPLATES_ELEMENTS_COLLECTION, LOGS_COLLECTION } from '../config/collections';
import { omit, orderBy } from 'lodash'

export function getTemplates() {
  return async (dispatch) => {
    try {
      let templates = []

      const result = await db.collection(TEMPLATES_COLLECTION).get()

      result.forEach(doc => {
        templates = [...templates, { id: doc.id, ...doc.data() }]
      });

      dispatch(setDataListTemplates(templates))
    } catch (error) {
      console.log('get templates', error);
    }
  }
}

export function getTemplateById(templateId) {
  return async (dispatch) => {
    try {
      const template = await db.collection(TEMPLATES_COLLECTION).doc(templateId).get()

      const data = template.data()
      data.pages = await getPagesByTemplate(templateId)
      data.pages = orderBy(data.pages, 'position_list')

      dispatch(setDataTemplateToEdit(data))
    } catch (error) {
      console.log('get template by id', error)
    }
  }
}

async function getPagesByTemplate(templateId) {
  const sections = []
  const pagesByTemplate = await db.collection(TEMPLATES_COLLECTION)
    .doc(templateId).collection(TEMPLATES_PAGES_COLLECTION).get()


  await asyncForEach(pagesByTemplate.docs, async (doc) => {
    const page = doc.data()

    page.id = doc.id
    page.elements = await getQuestionsByPage(templateId, doc.id)

    sections.push(page)
  })

  return sections
}


async function getQuestionsByPage(templateId, pageId) {
  const questions = []
  const data = await db.collection(TEMPLATES_COLLECTION)
    .doc(templateId).collection(TEMPLATES_PAGES_COLLECTION)
    .doc(pageId).collection(TEMPLATES_ELEMENTS_COLLECTION).get()

  await asyncForEach(data.docs, async (doc) => {
    questions.push({ id: doc.id, ...doc.data() })
  })

  return questions
}

export function createTemplate(template) {
  return async (dispatch, getState) => {
    try {
      const { user } = getState().userReducer

      const pages = template.pages

      template.ownerId = user.id
      template.editedBy = user.id

      const newTemplate = omit(template, ['pages'])

      const result = await db.collection(TEMPLATES_COLLECTION).add(newTemplate)
      newTemplate.id = result.id
      await db.collection(TEMPLATES_COLLECTION).doc(result.id).set(newTemplate)

      savePages(pages, result.id)

      const log = createLog('create', user)
      await db.collection(TEMPLATES_COLLECTION).doc(result.id).collection(LOGS_COLLECTION).doc().set(log)
    } catch (error) {
      console.log('save template', error)
    }
  }
}

function savePages(pages, templateId) {
  pages.forEach(async (page) => {
    const questions = page.elements
    const newPage = omit(page, ['elements'])
    const pageStored = await db.collection(TEMPLATES_COLLECTION).doc(templateId).collection(TEMPLATES_PAGES_COLLECTION).add(newPage)

    saveQuestions(questions, templateId, pageStored.id)
  })
}

function saveQuestions(questions, templateId, pageId) {
  questions.forEach(async (question) => {
    await db.collection(TEMPLATES_COLLECTION)
      .doc(templateId)
      .collection(TEMPLATES_PAGES_COLLECTION)
      .doc(pageId)
      .collection(TEMPLATES_ELEMENTS_COLLECTION)
      .add(question)
  })
}

export function updateTemplate(template) {
  return async (dispatch, getState) => {
    try {
      const { user } = getState().userReducer

      const pages = template.pages
      const newTemplate = omit(template, ['pages'])
      await db.collection(TEMPLATES_COLLECTION).doc(newTemplate.id).update(newTemplate)

      updatePages(pages, newTemplate.id)

      const log = createLog('update', user)
      await db.collection(TEMPLATES_COLLECTION).doc(newTemplate.id).collection(LOGS_COLLECTION).doc().set(log)

      dispatch(setDataTemplateToEdit({}))
    } catch (error) {
      console.log('delete template', error);
    }
  }
}

function updatePages(pages, templateId) {
  pages.forEach(async (page) => {
    const questions = page.elements
    const newPage = omit(page, ['elements'])
    if (typeof newPage.id == "string") {
      await db.collection(TEMPLATES_COLLECTION).doc(templateId).collection(TEMPLATES_PAGES_COLLECTION).doc(newPage.id).update(newPage)
      updateQuestions(questions, templateId, newPage.id)
    } else {
      const pageStored = await db.collection(TEMPLATES_COLLECTION).doc(templateId).collection(TEMPLATES_PAGES_COLLECTION).add(newPage)

      saveQuestions(questions, templateId, pageStored.id)
    }
  })
}

export function updateQuestions(questions, templateId, pageId) {
  questions.forEach(async (question) => {
    if (question.id) {
      await db.collection(TEMPLATES_COLLECTION).doc(templateId)
        .collection(TEMPLATES_PAGES_COLLECTION).doc(pageId)
        .collection(TEMPLATES_ELEMENTS_COLLECTION).doc(question.id).update(question)
    } else {
      saveQuestions([question], templateId, pageId)
    }
  })
}

export function deleteTemplate(template) {
  return async (dispatch, getState) => {
    try {
      const { user } = getState().userReducer
      const { templates } = getState().templatesReducer
      const newTemplates = templates.filter(item => item.id !== template.id)

      dispatch(setDataListTemplates(newTemplates))

      await db.collection(TEMPLATES_COLLECTION).doc(template.id).update({ isActived: false })

      const log = createLog('delete', user)

      await db.collection(TEMPLATES_COLLECTION).doc(template.id).collection(LOGS_COLLECTION).doc().set(log)
    } catch (error) {
      console.log('delete template', error);
    }
  }
}

function createLog(action, user) {
  return {
    action,
    date_log: new Date(),
    user_id: user.id
  }
}

async function asyncForEach(array, callback) {
  for (let index = 0; index < array.length; index++) {
    await callback(array[index], index, array);
  }
}

const setDataListTemplates = (payload) => ({
  type: SET_DATA_LIST_TEMPLATES,
  payload
});

export const setDataTemplateToEdit = (payload) => ({
  type: SET_DATA_TEMPLATE_TO_EDIT,
  payload
});