import { authentication, firestore as db } from '../config/firebase'
import { SET_DATA_USER, SET_DATA_LIST_USERS, SET_USER_TO_EDIT } from './actiontypes'
import { USER_COLLECTION, LOGS_COLLECTION } from '../config/collections';

export function userLogin(email, password) {
  return async (dispatch) => {
    try {
      const { user } = await authentication.signInWithEmailAndPassword(email, password)

      const result = await db.collection(USER_COLLECTION).doc(user.uid).get()

      if (result.exists) {
        const dataUser = result.data()
        localStorage.setItem("user", JSON.stringify(dataUser))
        dispatch(setDataUser(dataUser))

        return dataUser
      }
    } catch (error) {
      console.log('user login', error);
      throw new Error(error.code)
    }
  };
}

export function getUsers() {
  return async (dispatch, getState) => {
    try {
      let users = []

      const result = await db.collection(USER_COLLECTION).get()

      result.forEach(doc => {
        const user = { id: doc.id, ...doc.data() }
        users = [user, ...users]
      });

      dispatch(setDataListUsers(users))
    } catch (error) {
      console.log('get users', error);
    }
  }
}

export function createUser(data) {
  return async (dispatch) => {
    try {
      const randomPassword = Math.random().toString(36).slice(-8);
      const { user } = await authentication.createUserWithEmailAndPassword(data.email, randomPassword)
      await authentication.sendPasswordResetEmail(user.email)

      data.id = user.uid
      data.state = true

      await db.collection(USER_COLLECTION).doc(user.uid).set(data)
    } catch (error) {
      console.log('create user', error);
      throw new Error(error.code)
    }
  }
}

export function updateUser(data) {
  return async (_, getState) => {
    try {
      const { user } = getState().userReducer

      await db.collection(USER_COLLECTION).doc(data.id).update(data)

      const log = createLog('update', user)
  
      await db.collection(USER_COLLECTION).doc(data.id).collection(LOGS_COLLECTION).doc().set(log)
    } catch (error) {
      console.log('edit user', error);
    }
  }
}

export function deleteUser(data) {
  return async (dispatch, getState) => {
    try {
      const { users, user } = getState().userReducer
      const newUsers = users.filter(user => user.id !== data.id)

      dispatch(setDataListUsers(newUsers))
      
      data.state = false
      await db.collection(USER_COLLECTION).doc(data.id).update(data)

      const log = createLog('delete', user)
  
      await db.collection(USER_COLLECTION).doc(data.id).collection(LOGS_COLLECTION).doc().set(log)
    } catch (error) {
      console.log('delete user', error);
    }
  }
}

function createLog(action, user) {
  return {
    action,
    date_log: new Date(),
    user_id: user.id
  }
}

export const setDataUser = (payload) => ({
  type: SET_DATA_USER,
  payload
});

const setDataListUsers = (payload) => ({
  type: SET_DATA_LIST_USERS,
  payload
});

export const setUserToEdit = (payload) => ({
  type: SET_USER_TO_EDIT,
  payload
})