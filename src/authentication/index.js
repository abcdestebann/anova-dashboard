import React, { Component } from 'react';
import { Redirect, Route } from 'react-router-dom';
import { connect } from 'react-redux'
import { setDataUser } from '../actions/user'

class VerifyAuthentication extends Component {

   getUser = () => {
      const user = JSON.parse(localStorage.getItem("user"))

      if (user) {
         this.props.dispatch(setDataUser(user))
         return true
      }
      
      return false
   }

   render() {
      const { component: Component, ...rest } = this.props

      return (
         <Route
            {...rest}
            render={props =>
               this.getUser()
                  ? (<Component {...props} />)
                  : (<Redirect to={{
                     pathname: "/login",
                     state: { from: props.location }
                  }} />)
            }
         />
      )
   }
}

export default connect()(VerifyAuthentication)