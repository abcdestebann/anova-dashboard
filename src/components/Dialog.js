import React from 'react'
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Button from '@material-ui/core/Button';

const CustomDialog = ({ open, message, onClickCancel, onClickAccept, children }) => {
  return (
    <Dialog
      open={open}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description">
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {children}
          {message}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        {onClickCancel && <Button onClick={onClickCancel} color="primary"> Cancelar </Button>}
        {onClickAccept && <Button onClick={onClickAccept} color="secondary"> Aceptar </Button>}
      </DialogActions>
    </Dialog>
  )
}

export default CustomDialog