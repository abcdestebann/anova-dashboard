import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import classNames from 'classnames';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import Menu from '@material-ui/core/Menu';
import MenuItem from '@material-ui/core/MenuItem';

const Header = ({ classes, open, toogleDrawer, logoutUser }) => {

   const [anchorEl, setAnchorEl] = React.useState(null);

   const handleClick = (event) => {
      setAnchorEl(event.currentTarget);
   };

   const handleClose = () => {
      setAnchorEl(null);
   };

   return (
      <AppBar
         position="fixed"
         className={classNames(classes.appBar, {
            [classes.appBarShift]: open,
         })}>
         <Toolbar disableGutters={!open}>
            <IconButton
               edge="start"
               color="inherit"
               aria-label="Open drawer"
               onClick={() => toogleDrawer(true)}
               className={classNames(classes.menuButton, open && classes.hide)}>
               <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" noWrap className={classes.title}>Trek Dashboard</Typography>

            <IconButton
               color="inherit"
               onClick={handleClick}>
               <AccountCircle fontSize="large" />
            </IconButton>

         </Toolbar>

         <Menu
            id="simple-menu"
            anchorEl={anchorEl}
            keepMounted
            open={Boolean(anchorEl)}
            onClose={handleClose}>
            <MenuItem onClick={logoutUser}>Cerrar sesión</MenuItem>
         </Menu>

      </AppBar>
   )

}



export default Header
