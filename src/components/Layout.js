import React, { useState } from 'react'
import { connect } from 'react-redux'
import { useHistory } from "react-router-dom";

// ROUTER
import { Switch, Route, Redirect } from 'react-router-dom';
import routes from '../routes';

import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import Header from './Header';
import Sidebar from './Sidebar';

const switchRoutes = (
   <Switch>
      {routes.map((prop, key) => {
         if (prop.redirect)
            return <Redirect from={prop.path} to={prop.to} key={key} />;
         return <Route path={prop.path} component={prop.component} key={key} />;
      })}
   </Switch>
);

const Layout = ({
   classes,
   theme,
   location,
   user
}) => {
   const history = useHistory();
   const [open, toogleDrawer] = useState(true);

   const logoutUser = () => {
      localStorage.removeItem("user")
      history.push('/')
   }

   return (
      <div className={classes.root}>
         <CssBaseline />
         <Header
            classes={classes}
            open={open}
            toogleDrawer={toogleDrawer}
            logoutUser={logoutUser} />
         <Sidebar
            classes={classes}
            routes={routes}
            open={open}
            theme={theme}
            toogleDrawer={toogleDrawer}
            location={location}
            userProfile={user.profile} />
         <main
            className={classNames(classes.content, {
               [classes.contentShift]: open,
            })} >
            <div className={classes.drawerHeader} />
            {switchRoutes}
         </main>
      </div>
   );
}

const drawerWidth = 240;

const styles = theme => ({
   root: {
      display: 'flex',
   },
   appBar: {
      transition: theme.transitions.create(['margin', 'width'], {
         easing: theme.transitions.easing.sharp,
         duration: theme.transitions.duration.leavingScreen,
      }),
   },
   appBarShift: {
      width: `calc(100% - ${drawerWidth}px)`,
      marginLeft: drawerWidth,
      transition: theme.transitions.create(['margin', 'width'], {
         easing: theme.transitions.easing.easeOut,
         duration: theme.transitions.duration.enteringScreen,
      }),
   },
   menuButton: {
      marginLeft: 12,
      marginRight: 20,
   },
   hide: {
      display: 'none',
   },
   drawer: {
      width: drawerWidth,
      flexShrink: 0,
   },
   drawerPaper: {
      width: drawerWidth,
   },
   drawerHeader: {
      display: 'flex',
      alignItems: 'center',
      padding: '0 8px',
      ...theme.mixins.toolbar,
      justifyContent: 'center',
      position: 'relative'
   },
   content: {
      flexGrow: 1,
      padding: theme.spacing(3),
      transition: theme.transitions.create('margin', {
         easing: theme.transitions.easing.sharp,
         duration: theme.transitions.duration.leavingScreen,
      }),
      marginLeft: -drawerWidth,
   },
   contentShift: {
      transition: theme.transitions.create('margin', {
         easing: theme.transitions.easing.easeOut,
         duration: theme.transitions.duration.enteringScreen,
      }),
      marginLeft: 0,
   },
   item: {
      position: "relative",
      display: "block",
      textDecoration: "none",
      "&:hover,&:focus,&:visited,&": {
         color: theme.palette.primary.main
      }
   },
   title: {
      flexGrow: 1,
   },
   logo: {
      height: 40
   },
   iconBack: {
      position: 'absolute',
      right: 0
   }
});

const mapStateToProps = ({ userReducer }) => ({
   user: userReducer.user
})

export default connect(mapStateToProps)(withStyles(styles, { withTheme: true })(Layout))