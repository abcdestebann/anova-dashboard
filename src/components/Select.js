import React from 'react'
import Select from '@material-ui/core/Select';
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";

const CustomSelect = ({ value, options, handleChangeSelect, label, styleContainer, name, disabled }) => {
  return (
    <FormControl variant="outlined" className={styleContainer}>
      <InputLabel id="demo-simple-select-outlined-label">{label}</InputLabel>
      <Select
        labelId="demo-simple-select-outlined-label"
        id="demo-simple-select-outlined"
        color="secondary"
        value={value}
        name={name}
        disabled={disabled}
        onChange={handleChangeSelect}
        label={label}>
        {
          options.map((response) =>
            <MenuItem key={response.id} value={response.id}>{response.title || response.name}</MenuItem>)
        }
      </Select>
    </FormControl>
  )
}

export default CustomSelect