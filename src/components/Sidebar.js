import React from 'react'
import { NavLink } from 'react-router-dom';
import Drawer from '@material-ui/core/Drawer';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import List from '@material-ui/core/List';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import logo from '../assets/logo.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

const activeRoute = (location, routeName) => location.pathname.indexOf(routeName) > -1;

const Sidebar = ({ classes, theme, open, toogleDrawer, routes, location, userProfile }) => {
   return (
      <Drawer
         className={classes.drawer}
         variant="persistent"
         anchor="left"
         open={open}
         classes={{
            paper: classes.drawerPaper,
         }}
      >
         <div className={classes.drawerHeader}>
            <img src={logo} alt="logo" className={classes.logo} />
            <IconButton onClick={() => toogleDrawer(false)} className={classes.iconBack}>
               {theme.direction === 'ltr' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
            </IconButton>
         </div>
         <Divider />
         <List>
            {routes.map((route, index) => {
               if (route.navbarName && route.users.indexOf(userProfile) !== -1) {
                  const isActive = activeRoute(location, route.path)
                  if (route.redirect) return null;
                  return (
                     <NavLink to={route.path} className={classes.item} activeClassName="active" key={route.sidebarName}>
                        <ListItem button selected={isActive} key={route.navbarName}>
                           <ListItemIcon style={{ justifyContent: 'center' }}>
                              <FontAwesomeIcon style={{ fontSize: 20, color: isActive ? '#EC6642' : '#50637d' }} icon={route.icon} />
                           </ListItemIcon>
                           <ListItemText style={{ color: isActive ? '#EC6642' : 'black' }} primary={route.navbarName} />
                        </ListItem>
                     </NavLink>
                  )
               }
            })}
         </List>
         <Divider />
      </Drawer>
   )
}

export default Sidebar