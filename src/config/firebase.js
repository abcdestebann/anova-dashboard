import * as firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'

const firebaseConfig = {
  apiKey: "AIzaSyBIOACEue_LhWJO1zhAVURStixtolfED5o",
  authDomain: "anovamobile-dev.firebaseapp.com",
  databaseURL: "https://anovamobile-dev.firebaseio.com",
  projectId: "anovamobile-dev",
  storageBucket: "anovamobile-dev.appspot.com",
  messagingSenderId: "320202530714",
  appId: "1:320202530714:web:2cf723dc0cb2fae96a6423"
};

const app = firebase.initializeApp(firebaseConfig)

const authentication = app.auth()
const firestore = app.firestore()

export { authentication, firestore } 