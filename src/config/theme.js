export default {
  palette: {
     primary: {
        dark: '#FFF',
        main: '#1C2633',
        light: '#FFF',
        contrastText: '#FFF',
     },
     secondary: {
        dark: '#c25537',
        main: '#EC6642',
        light: '#ff8260',
        contrastText: '#FFF',
     },
     accent: {
       dark: '#FFF',
       main: '#EC6642',
       light: '#FFF',
       contrastText: '#FFF'
     },
     error: {
        main: '#f44336',
        contrastText: '#FFF',
     },
     action: {
      disabledBackground: '#D4D3D3',
      disabled: '#BBBBBB'
    }
  },
  typography: {
   fontFamily: `"Montserrat", sans-serif`,
   useNextVariants: true,
  }
}