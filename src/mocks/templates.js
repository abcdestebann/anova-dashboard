export const SECTIONS = [
  {
    date_created: null,
    isActive: true,
    position_list: 1,
    title: "",
    elements: [
      {
        date_created: null,
        isActive: true,
        score: null,
        title: '',
        type: null
      }
    ]
  }
]

export const RESPONSES = [
  {
    id: 1,
    response: "Texto"
  },
  {
    id: 2,
    response: "Número"
  },
  {
    id: 3,
    response: "Checkbox"
  },
  {
    id: 4,
    response: "Fecha"
  },
  {
    id: 5,
    response: "Instrucción"
  },
  {
    id: 6,
    response: "options",
    options: {
      descOption: 'Hecho',
      descOption2: 'Por hacer'
    }
  },
  {
    id: 7,
    response: "options",
    options: {
      descOption: 'Bueno',
      descOption2: 'Regular',
      descOption3: 'Pobre',
    }
  },
  {
    id: 8,
    response: "options",
    options: {
      descOption: 'Salvo',
      descOption2: 'En riesgo'
    }
  },
  {
    id: 9,
    response: "options",
    options: {
      descOption: 'Pasó',
      descOption2: 'Falló'
    }
  },
]