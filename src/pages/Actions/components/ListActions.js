import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import { Typography } from '@material-ui/core';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  containerEmpty: {
    width: '100%',
    textAlign: 'center',
    marginTop: 24
  }
});

const ListActions = ({ actions, users, showDialogInfo }) => {
  const classes = useStyles();

  const getUserRow = (userId) => {
    const user = users.find(template => template.id === userId)

    if (user) {
      return `${user.name.first} ${user.name.last}`
    } else {
      return "No se encontró el usuario"
    }
  }

  const parseDate = (seconds) => {
    const date = new Date(seconds * 1000)
    const newDate = `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`
    return newDate
  }

  if (actions.length > 0) {
    return (
      <TableContainer style={{ marginTop: 16 }} component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Titulo</TableCell>
              <TableCell align="center">Usuario asignado</TableCell>
              <TableCell align="center">Estado</TableCell>
              <TableCell align="center">Prioridad</TableCell>
              <TableCell align="center">Fecha vencimiento</TableCell>
              <TableCell align="center">Creado por</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {actions.map((action) => {
              return (
                <TableRow onClick={showDialogInfo} key={action.id} style={{ cursor: "pointer" }}>
                  <TableCell component="th" scope="row">{action.title}</TableCell>
                  <TableCell align="center">{getUserRow(action.userAssignedId)}</TableCell>
                  <TableCell align="center">{action.status}</TableCell>
                  <TableCell align="center">{action.priority}</TableCell>
                  <TableCell align="center">{parseDate(action.expirationDate.seconds)}</TableCell>
                  <TableCell align="center">{getUserRow(action.createdBy)}</TableCell>
                </TableRow>
              )
            })}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }

  return (
    <div className={classes.containerEmpty}>
      <Typography variant="h5">No hay accciones creadas</Typography>
    </div>
  )
}

export default ListActions