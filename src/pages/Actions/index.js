import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles';
import { connect } from 'react-redux'
import { getUsers } from '../../actions/user'
import { getActions } from '../../actions/actions'
import ListActions from './components/ListActions'
import CustomDialog from '../../components/Dialog'
import Smartphone from "@material-ui/icons/Smartphone";
import CircularProgress from '@material-ui/core/CircularProgress';

class Actions extends Component {

  state = {
    openDialogInfo: false
  }

  async componentDidMount() {
    await this.props.dispatch(getUsers())
    await this.props.dispatch(getActions())
  }

  toggleDialogInfo = (open) => {
    this.setState({ openDialogInfo: open })
  }

  render() {
    const { users, actions, classes } = this.props;

    if (actions.length === 0) {
      return (
        <div className={classes.containerLoading}>
          <CircularProgress color="secondary" />
        </div>
      )
    }

    return (
      <div>
        <ListActions actions={actions} users={users} showDialogInfo={this.toggleDialogInfo.bind(null, true)} />
        <CustomDialog
          open={this.state.openDialogInfo}
          message="El detalle de las acciones sólo se pueden ver desde la aplicación móvil"
          onClickAccept={this.toggleDialogInfo.bind(null, false)}>
          <div className={classes.containerChildrenDialog}>
            <Smartphone fontSize="large" />
          </div>
        </CustomDialog>
      </div>
    )
  }
}

const styles = (theme) => ({
  containerChildrenDialog: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 16
  },
  containerLoading: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh'
  }
})

const mapStateToProps = ({ userReducer, actionsReducer }) => ({
  users: userReducer.users,
  actions: actionsReducer.actions
})

export default connect(mapStateToProps)(withStyles(styles)(Actions))