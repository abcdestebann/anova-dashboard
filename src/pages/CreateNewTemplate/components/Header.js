import React from 'react'
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import ArrowBack from '@material-ui/icons/ArrowBack';
import { withStyles } from '@material-ui/core/styles';
import Save from "@material-ui/icons/Save";
import Button from '@material-ui/core/Button';

const Header = ({ handleGoBack, classes, disableButton = false, handleButtonSave }) => {
  return (
    <AppBar position="static">
      <Toolbar>
        <IconButton
          onClick={handleGoBack}
          edge="start"
          className={classes.menuButton}
          color="inherit">
          <ArrowBack />
        </IconButton>
        <Typography className={classes.title}>Volver a la lista</Typography>

        <Button
          onClick={handleButtonSave}
          size="small"
          variant="contained"
          color="secondary"
          disabled={disableButton}
          startIcon={<Save />}>
          Guardar
        </Button>
      </Toolbar>
    </AppBar>
  )
}

const styles = (theme) => ({
  root: {
    flexGrow: 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  title: {
    flexGrow: 1,
  },
  disabledButton: {
    backgroundColor: 'red'
  }
})

export default withStyles(styles)(Header)  
