import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Select from '@material-ui/core/Select';
import InputLabel from "@material-ui/core/InputLabel";
import MenuItem from "@material-ui/core/MenuItem";
import FormControl from "@material-ui/core/FormControl";
import Chip from '@material-ui/core/Chip';

import { RESPONSES } from '../../../mocks/templates'

class QuestionSectionTemplate extends Component {

  handleChageText = (event) => {
    const { value } = event.target
    const { index, handleChangeTextQuestion } = this.props;

    handleChangeTextQuestion(index, value)
  }

  handleChangeType = (event) => {
    const { value } = event.target
    const { index, handleSelectTypeOfQuestion } = this.props;

    handleSelectTypeOfQuestion(index, value)
  }

  renderOptionsResponse = (options) => {
    let text = ""

    Object.values(options).forEach(option => {
      if (text === "") {
        text = option
      } else {
        text = `${text} / ${option}`
      }
    })

    return text
  }

  render() {
    const { classes, index, handleDeleteQuestion, question } = this.props;

    return (
      <div className={classes.container}>
        <TextField
          color="secondary"
          placeholder="Escribe la pregunta"
          name="textQuestion"
          value={question.title}
          onChange={this.handleChageText}
          className={classes.input}
          variant="outlined"
        />
        <div className={classes.containerResponse}>
          <FormControl variant="outlined" className={classes.formControl}>
            <InputLabel id="demo-simple-select-outlined-label">Response</InputLabel>
            <Select
              labelId="demo-simple-select-outlined-label"
              id="demo-simple-select-outlined"
              color="secondary"
              value={question.type}
              onChange={this.handleChangeType}
              label="Respuesta">
              {RESPONSES.map((response) =>
                <MenuItem key={response.id} value={response.id}>
                  {
                    response.options
                      ? this.renderOptionsResponse(response.options)
                      : response.response
                  }
                </MenuItem>)}
            </Select>
          </FormControl>
        </div>

        {index !== 0 &&
          <IconButton onClick={handleDeleteQuestion} className={classes.iconDelete} aria-label="delete">
            <DeleteIcon />
          </IconButton>}
      </div>
    )
  }
}

const styles = (theme) => ({
  container: {
    display: 'flex',
    position: 'relative',
    marginTop: theme.spacing(3)
  },
  input: {
    flex: 2,
    marginRight: theme.spacing(2)
  },
  containerResponse: {
    flex: 1
  },
  iconDelete: {
    color: '#f44336',
    position: 'absolute',
    right: 0,
  },
  formControl: {
    width: '85%'
  },
})

export default withStyles(styles)(QuestionSectionTemplate)