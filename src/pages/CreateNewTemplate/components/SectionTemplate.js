import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import DeleteIcon from '@material-ui/icons/Delete';
import Button from '@material-ui/core/Button';
import Add from "@material-ui/icons/Add";

import TitleTemplate from './TitleTemplate';
import QuestionSectionTemplate from './QuestionSectionTemplate'

class SectionTemplate extends Component {

  state = {
    toggleInputTitle: false,
    questions: []
  }

  componentDidMount() {
    this.setState({ questions: this.props.questions })
  }

  handleToggleInput = (input) => {
    this.setState({ toggleInputTitle: !this.state.toggleInputTitle, })
  }

  handleChageText = (event) => {
    const { value } = event.target
    const { index, handleChangeTitleSection } = this.props;

    handleChangeTitleSection(index, value)
  }

  handleAddQuesion = () => {
    const question = {
      date_created: new Date(),
      isActive: true,
      score: null,
      title: '',
      type: null
    }
    const questions = [...this.state.questions, question]
    this.setState({ questions })
  }

  handleDeleteQuestion = (index) => {
    const { index: indexSection, handleUpdateSection } = this.props
    const questions = [...this.state.questions]
    questions[index].isActive = false

    this.setState({ questions })
    handleUpdateSection(indexSection, questions)
  }

  handleChangeTextQuestion = (index, value) => {
    const { index: indexSection, handleUpdateSection } = this.props
    const questions = [...this.state.questions]
    questions[index].title = value

    this.setState({ questions })
    handleUpdateSection(indexSection, questions)
  }

  handleSelectTypeOfQuestion = (index, type) => {
    const { index: indexSection, handleUpdateSection } = this.props
    const questions = [...this.state.questions]
    questions[index].type = type

    this.setState({ questions })
    handleUpdateSection(indexSection, questions)
  }

  render() {
    const { classes, handleDeleteSection, index, sectionExpanded, section } = this.props;
    const { toggleInputTitle, questions } = this.state

    return (
      <div className={classes.root}>

        <ExpansionPanel
          className={classes.expansionPanel}
          expanded={sectionExpanded === index && !toggleInputTitle}
          onChange={this.props.handleExpand(index)}>
          <ExpansionPanelSummary expandIcon={<ExpandMoreIcon />}>
            <TitleTemplate
              title={section.title}
              toggleInput={toggleInputTitle}
              nameToggleInput="toggleInputTitle"
              handleToggleInput={this.handleToggleInput}
              handleChageText={this.handleChageText}
              defaultText="Nombre de página"
              variant="h6"
              name="title"
            />
          </ExpansionPanelSummary>

          <ExpansionPanelDetails className={classes.expasionDetails}>

            <div style={{ display: 'flex' }}>
              <div className={classes.headerQuestion}>
                <Typography variant="body1">Pregunta</Typography>
              </div>

              <div className={classes.headerResponse}>
                <Typography variant="body1">Tipo de respuesta</Typography>
              </div>
            </div>

            <div style={{ marginTop: 16 }}>
              {questions.map((question, index) => {
                if (question.isActive) {
                  return <QuestionSectionTemplate
                    key={index}
                    index={index}
                    question={question}
                    handleChangeTextQuestion={this.handleChangeTextQuestion}
                    handleSelectTypeOfQuestion={this.handleSelectTypeOfQuestion}
                    handleDeleteQuestion={this.handleDeleteQuestion.bind(null, index)}
                  />
                }
              })}
            </div>

            <Button
              onClick={this.handleAddQuesion}
              size="small"
              variant="text"
              color="secondary"
              className={classes.button}
              startIcon={<Add />}>
              Agregar pregunta
            </Button>

          </ExpansionPanelDetails>
        </ExpansionPanel>

        <IconButton onClick={handleDeleteSection} className={classes.iconDelete} aria-label="delete">
          <DeleteIcon />
        </IconButton>
      </div>
    )
  }
}

const styles = (theme) => ({
  root: {
    width: '100%',
    position: 'relative',
    marginTop: theme.spacing(2),
    display: 'flex'
  },
  expansionPanel: {
    flex: 1
  },
  expasionDetails: {
    borderTop: '1px solid #e8e7e7',
    display: 'block'
  },
  heading: {
    fontSize: theme.typography.pxToRem(15),
    fontWeight: theme.typography.fontWeightRegular,
  },
  iconDelete: {
    position: 'absolute',
    right: -50,
    color: '#f44336'
  },
  headerQuestion: {
    flex: 2,
    display: 'flex'
  },
  headerResponse: {
    flex: 1,
    display: 'flex'
  },
  button: {
    marginTop: theme.spacing(2),
  }
})


export default withStyles(styles)(SectionTemplate)
