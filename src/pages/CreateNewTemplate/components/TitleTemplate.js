import React from 'react'
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Create from "@material-ui/icons/Create";
import Done from "@material-ui/icons/Done";

const TitleTemplate = ({
  title,
  toggleInput,
  nameToggleInput,
  handleToggleInput,
  handleChageText,
  defaultText,
  variant,
  name,
  classes
}) => {
  return (
    <div className={classes.container}>

      {!toggleInput &&
        <div
          onClick={handleToggleInput.bind(null, nameToggleInput)}
          className={classes.titleContainer}>
          <Typography variant={variant} className={classes.title}>{title || defaultText}</Typography>
          <Create />
        </div>}

      {toggleInput &&
        <div className={classes.inputContainer}>
          <TextField
            color="secondary"
            name={name}
            value={title}
            onChange={handleChageText}
            className={classes.input}
          />
          <Done className={classes.iconDone} onClick={handleToggleInput.bind(null, nameToggleInput)} />
        </div>}

    </div>
  )
}

const styles = (theme) => ({
  container: {
    width: 'fit-content'
  },
  titleContainer: {
    display: 'flex',
    alignItems: 'center',
    cursor: 'pointer'
  },
  title: {
    marginRight: 10
  },
  inputContainer: {
    display: 'flex',
    alignItems: 'center',
    width: 200
  },
  input: {
    marginRight: 10
  },
  iconDone: {
    cursor: 'pointer',
    color: '#8bc34a'
  }
})

export default withStyles(styles)(TitleTemplate)
