import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import Add from "@material-ui/icons/Add";
import CircularProgress from '@material-ui/core/CircularProgress';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Done from "@material-ui/icons/Done";

import Header from './components/Header';
import TitleTemplate from './components/TitleTemplate';
import SectionTemplate from './components/SectionTemplate';

import { SECTIONS } from '../../mocks/templates'
import { createTemplate, updateTemplate, setDataTemplateToEdit } from '../../actions/templates'
import { isEmpty } from 'lodash'

class CreateNewTemplate extends Component {

  container = null

  sectionDefault = {
    date_created: new Date(),
    isActive: true,
    position_list: 1,
    title: "",
    elements: [
      {
        date_created: new Date(),
        isActive: true,
        score: null,
        title: '',
        type: null
      }
    ]
  }

  state = {
    title: "",
    description: "",
    toggleInputTitle: false,
    toggleInputDescription: false,
    sections: [],
    sectionExpanded: false,
    saving: false,
    openDialogSuccess: false
  }

  async componentDidMount() {
    const { templateToEdit } = this.props
    if (!isEmpty(templateToEdit)) {
      this.setState({
        title: templateToEdit.title,
        description: templateToEdit.description,
        sections: templateToEdit.pages
      })
    } else {
      this.handleCreateTemplate(false)
      this.setState({
        sections: [this.sectionDefault]
      })
    }
  }

  componentDidUpdate() {
    this.container.scrollIntoView({ behavior: "smooth" })
  }

  componentWillUnmount() {
    this.props.dispatch(setDataTemplateToEdit({}))
  }

  handleButtonSave = () => {
    if (isEmpty(this.props.templateToEdit)) {
      this.handleCreateTemplate()
    } else {
      this.handleUpdateTemplate()
    }
  }

  handleCreateTemplate = async (showDialog = true) => {
    if (showDialog) { this.setState({ saving: true }) }

    const data = {
      company: '',
      date_created: new Date(),
      industry: '',
      isActived: true,
      isPublic: true,
      last_modified: new Date(),
      project: '',
      title: this.state.title || "Titulo Plantilla",
      description: this.state.description,
      pages: this.state.sections
    }

    await this.props.dispatch(createTemplate(data))
    if (showDialog) {
      this.setState({ saving: false, openDialogSuccess: true })
    }
  }

  handleUpdateTemplate = async () => {
    const { templateToEdit } = this.props;
    const templateToUpdate = { ...templateToEdit }

    templateToUpdate.title = this.state.title
    templateToUpdate.description = this.state.description
    templateToUpdate.pages = this.state.sections

    await this.props.dispatch(updateTemplate(templateToUpdate))
    this.setState({ saving: false, openDialogSuccess: true })
  }

  handleGoBack = () => {
    this.props.history.goBack();
  }

  handleExpand = (panel) => (event, isExpanded) => {
    this.setState({ sectionExpanded: isExpanded ? panel : false })
  };

  handleToggleInput = (input) => {
    this.setState({ [input]: !this.state[input] })
  }

  handleChageText = (event) => {
    const { name, value } = event.target
    this.setState({ [name]: value })
  }

  handleAddSection = () => {
    const section = {
      id: Math.random() * (100000 - 1) + 1,
      title: "",
      position_list: this.state.sections.length + 1,
      date_created: new Date(),
      isActive: true,
      elements: [
        {
          date_created: new Date(),
          isActive: true,
          score: null,
          title: '',
          type: null
        }
      ]
    }
    const sections = [...this.state.sections, section]
    this.setState({ sections })
  }

  handleChangeTitleSection = (index, value) => {
    const sections = [...this.state.sections]
    sections[index].title = value
    this.setState({ sections })
  }

  handleDeleteSection = (index) => {
    const sections = [...this.state.sections]
    sections[index].isActive = false

    this.setState({ sections })
  }

  handleUpdateSection = (index, questions) => {
    const sections = [...this.state.sections]
    sections[index].elements = questions

    this.setState({ sections })
  }

  handleButtonDialog = () => {
    this.setState({ openDialogSuccess: false })
    this.props.history.goBack();
  }

  render() {
    const { classes } = this.props;
    const { title, description, toggleInputTitle, toggleInputDescription, sectionExpanded } = this.state;
    const { sections } = this.state;

    return (
      <div>
        <Header
          handleGoBack={this.handleGoBack}
          handleButtonSave={this.handleButtonSave}
          disableButton={!title} />
        <div ref={el => this.container = el} className={classes.container}>
          <Typography variant="h4" className={classes.title}>Creador de plantilla</Typography>
          <TitleTemplate
            title={title}
            toggleInput={toggleInputTitle}
            nameToggleInput="toggleInputTitle"
            handleToggleInput={this.handleToggleInput}
            handleChageText={this.handleChageText}
            defaultText="Título"
            variant="h5"
            name="title"
          />

          <div style={{ marginTop: 16 }}>
            <TitleTemplate
              title={description}
              toggleInput={toggleInputDescription}
              nameToggleInput="toggleInputDescription"
              handleToggleInput={this.handleToggleInput}
              handleChageText={this.handleChageText}
              defaultText="Descripción"
              variant="body1"
              name="description"
            />
          </div>

          <div className={classes.containerSections}>
            {sections.map((section, index) => {
              if (section.isActive) {
                return <SectionTemplate
                  key={index}
                  index={index}
                  section={section}
                  handleDeleteSection={this.handleDeleteSection.bind(null, index)}
                  sectionExpanded={sectionExpanded}
                  handleExpand={this.handleExpand}
                  questions={section.elements}
                  handleUpdateSection={this.handleUpdateSection}
                  handleChangeTitleSection={this.handleChangeTitleSection}
                />
              }
            })}
            <Button
              onClick={this.handleAddSection}
              size="small"
              variant="text"
              color="secondary"
              className={classes.button}
              startIcon={<Add />}>
              Agregar página
            </Button>
          </div>
        </div>
        {this.state.saving && <div className={classes.containerLoading}>
          <CircularProgress color="secondary" />
        </div>}

        <Dialog
          open={this.state.openDialogSuccess}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogContent className={classes.containerDialog}>
            <Done className={classes.iconDone} />
            <DialogContentText id="alert-dialog-description">
              Plantilla guardada satisfactoriamente
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleButtonDialog} color="secondary"> Aceptar </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}


const styles = (theme) => ({
  container: {
    marginTop: theme.spacing(4),
    marginLeft: theme.spacing(12),
    marginRight: theme.spacing(12),
  },
  title: {
    fontWeight: 'bold',
    marginBottom: theme.spacing(4),
  },
  containerSections: {
    marginTop: theme.spacing(4),
  },
  button: {
    marginTop: theme.spacing(2),
  },
  containerLoading: {
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
    background: 'rgba(28 38 51 / .5)',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  containerDialog: {
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column'
  },
  iconDone: {
    fontSize: '6em',
    color: '#8BC34A'
  },
})


const mapStateToProps = ({ templatesReducer }) => ({
  templateToEdit: templatesReducer.templateToEdit
})

export default connect(mapStateToProps)(withStyles(styles)(CreateNewTemplate))