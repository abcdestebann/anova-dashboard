import React from 'react'
import { withStyles } from '@material-ui/core/styles';
import logo from '../../../assets/anova.png'

const ContainerLogo = ({ classes }) => (
  <div className={classes.container}>
    <img src={logo} alt="logo" className={classes.logo} />
  </div>
)


const styles = theme => ({
  container: {
    flex: 1,
    background: theme.palette.primary.main,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  logo: {
    width: '100%',
    height: '100%',
    objectFit: 'cover'
  }
})

export default withStyles(styles)(ContainerLogo) 
