import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import { userLogin } from '../../../actions/user'
import { withRouter } from "react-router";
import CircularProgress from '@material-ui/core/CircularProgress';
import logo from '../../../assets/logo.png'

class FormLogin extends Component {

  state = {
    email: '',
    password: '',
    buttonDisabled: true,
    emailDoesNoExits: false,
    passwordIncorrect: false,
    loading: false
  }

  handleChangeText = (event) => {
    const { name, value } = event.target
    this.setState({ [name]: value, emailDoesNoExits: false, passwordIncorrect: false }, () => {
      this.toggleButton()
    })
  }

  toggleButton = () => {
    const { email, password } = this.state;
    const buttonDisabled = email && password ? false : true

    this.setState({ buttonDisabled })
  }

  handleLogin = async () => {
    this.setState({ loading: true })
    try {
      const { history } = this.props;
      const { email, password } = this.state;
      const user = await this.props.dispatch(userLogin(email, password))
      if (user) {
        history.push('/templates')
      }
      this.setState({ loading: false })
    } catch (error) {
      const errorString = error.toString()
      if (errorString.includes("user-not-found")) {
        this.setState({ emailDoesNoExits: true })
      }
      if (errorString.includes("wrong-password")) {
        this.setState({ passwordIncorrect: true })
      }
      this.setState({ loading: false })
    }
  }

  render() {
    const { classes } = this.props;
    const { email, password, buttonDisabled, emailDoesNoExits, passwordIncorrect } = this.state

    return (
      <div className={classes.container}>
        <img src={logo} alt="logo" className={classes.logo} />

        <Typography variant="h4" color="secondary">Trek Dashboard</Typography>

        <TextField
          variant="outlined"
          value={email}
          name="email"
          label="Email"
          onChange={this.handleChangeText}
          color="secondary"
          className={classes.input} />
        {emailDoesNoExits && <div className={classes.divError}>
          <span className={classes.textError}>Correo electrónico no existe</span>
        </div>}


        <TextField
          variant="outlined"
          value={password}
          name="password"
          label="Password"
          onChange={this.handleChangeText}
          color="secondary"
          type="password"
          className={classes.input} />
        {passwordIncorrect && <div className={classes.divError}>
          <span className={classes.textError}>Contraseña inválida, vuelve a intentarlo</span>
        </div>}

        {!this.state.loading && <Button
          variant="contained"
          color="secondary"
          size="large"
          onClick={this.handleLogin}
          disabled={buttonDisabled}
          className={classes.button}>
          Iniciar sesión
        </Button>}

        {this.state.loading && <CircularProgress color="secondary" className={classes.button} />}
      </div>
    )
  }
}

const styles = theme => ({
  container: {
    flex: 1,
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'column'
  },
  input: {
    width: '60%',
    marginTop: 24
  },
  button: {
    width: '60%',
    marginTop: 64
  },
  divError: {
    width: '60%',
    marginTop: 8
  },
  textError: {
    fontSize: 14,
    color: '#f44336'
  },
  logo: {
    width: 120,
    marginLeft: 20,
    marginBottom: 24
  }
})

export default connect()(withStyles(styles)(withRouter(FormLogin))) 