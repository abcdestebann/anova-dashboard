import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles';
import ContainerLogo from './componentes/ContainerLogo';
import FormLogin from './componentes/FormLogin';

class Login extends Component {
  render() {
    const { classes } = this.props;

    return (
      <div className={classes.container}>
        <ContainerLogo />
        <FormLogin />
      </div>
    )
  }
}

const styles = theme => ({
  container: {
    height: '100vh',
    display: 'flex'
  }
})

export default withStyles(styles)(Login)