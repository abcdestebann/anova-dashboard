import React, { Component } from 'react'
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import CustomSelect from '../../../components/Select';
import TextField from '@material-ui/core/TextField';
import { KeyboardDatePicker } from '@material-ui/pickers';

class CreateSchedule extends Component {

  render() {
    const {
      open,
      templates = [],
      users = [],
      templateSelected,
      userSelected,
      title,
      dateSelected,
      scheduleToEdit,
      scheduleToSee,
      classes } = this.props

    return (
      <Dialog
        open={open}
        aria-labelledby="alert-dialog-title"
        aria-describedby="alert-dialog-description">
        <DialogTitle id="simple-dialog-title">{scheduleToEdit.id ? "Editar" : "Crear"} agenda</DialogTitle>
        <DialogContent className={classes.container}>

          <TextField
            color="secondary"
            placeholder="Título"
            name="title"
            value={title}
            onChange={this.props.handleChageText}
            className={classes.input}
            variant="outlined"
            disabled={scheduleToSee.id}
          />

          <KeyboardDatePicker
            disableToolbar
            variant="inline"
            format="D MMM, YYYY"
            margin="normal"
            id="date-picker-inline"
            label="Fecha"
            value={dateSelected}
            onChange={this.props.handleDateChange}
            className={classes.containerSelectDate}
            KeyboardButtonProps={{ 'aria-label': 'change date', }}
            disabled={scheduleToSee.id}
          />

          <CustomSelect
            label="Plantilla"
            value={templateSelected}
            options={templates}
            handleChangeSelect={this.props.handleChangeSelect}
            styleContainer={classes.containerSelect}
            name="templateSelected"
            disabled={scheduleToSee.id}
          />

          <CustomSelect
            label="Usuario"
            value={userSelected}
            options={users}
            handleChangeSelect={this.props.handleChangeSelect}
            styleContainer={classes.containerSelect}
            name="userSelected"
            disabled={scheduleToSee.id}
          />

        </DialogContent>
        <DialogActions>
          <Button onClick={this.props.handleClose.bind(null, false)} color="primary">
            {scheduleToSee.id ? 'Cerrar' : 'Cancelar'}
          </Button>

          {!scheduleToSee.id && <Button
            disabled={!title || !templateSelected || !userSelected}
            onClick={this.props.handleSaveSchedule}
            color="secondary">
            Guardar
          </Button>}
        </DialogActions>
      </Dialog>
    )
  }
}


const styles = () => ({
  container: {
    textAlign: 'center'
  },
  containerSelect: {
    width: 200,
    marginRight: 16,
    marginTop: 16
  },
  containerSelectDate: {
    width: 200,
    marginRight: 16,
    marginTop: 8
  },
  input: {
    width: 200,
    marginRight: 16
  }
})

export default withStyles(styles)(CreateSchedule) 