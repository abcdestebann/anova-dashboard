import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Delete from '@material-ui/icons/Delete';
import Create from "@material-ui/icons/Create";
import Visibility from "@material-ui/icons/Visibility";
import { Typography } from '@material-ui/core';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
  containerEmpty: {
    width: '100%',
    textAlign: 'center',
    marginTop: 24
  }
});

const ListSchedule = ({ schedules, users, templates, toggleDialogConfirm, handleEditSchedule, handleSeeSchedule }) => {
  const classes = useStyles();

  const getTemplateRow = (templateId) => {
    const template = templates.find(template => template.id === templateId)
    if (template) {
      return template.title
    } else {
      return "No se encontró la plantilla"
    }
  }

  const getUserRow = (userId) => {
    const user = users.find(template => template.id === userId)

    if (user) {
      return `${user.name.first} ${user.name.last}`
    } else {
      return "No se encontró el usuario"
    }
  }

  const parseDate = (date) => {
    if (date.seconds) {
      const seconds = date.seconds
      const newDate = new Date(seconds * 1000)
      const dateParse = `${newDate.getDate()}/${newDate.getMonth() + 1}/${newDate.getFullYear()}`
      return dateParse
    } else {
      const newDate = `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`
      return newDate
    }
  }

  if (schedules.length > 0) {
    return (
      <TableContainer style={{ marginTop: 16 }} component={Paper}>
        <Table className={classes.table} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Titulo</TableCell>
              <TableCell align="center">Plantilla asignada</TableCell>
              <TableCell align="center">Usuario asignado</TableCell>
              <TableCell align="center">Fecha</TableCell>
              <TableCell align="center">Opciones</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {schedules.map((schedule) => {
              if (schedule.isActived) {
                return (
                  <TableRow key={schedule.id}>
                    <TableCell component="th" scope="row">{schedule.title}</TableCell>
                    <TableCell align="center" size="medium">{getTemplateRow(schedule.templateId)}</TableCell>
                    <TableCell align="center">{getUserRow(schedule.userId)}</TableCell>
                    <TableCell align="center">{parseDate(schedule.date)}</TableCell>
                    <TableCell align="center">
                      <IconButton onClick={handleEditSchedule.bind(null, 'scheduleToSee', schedule)} >
                        <Visibility color="primary" style={{ color: '#2196F3' }} />
                      </IconButton>
                      <IconButton onClick={handleEditSchedule.bind(null, 'scheduleToEdit', schedule)}>
                        <Create color="primary" />
                      </IconButton>
                      <IconButton onClick={toggleDialogConfirm.bind(null, true, schedule)}>
                        <Delete color="error" />
                      </IconButton>
                    </TableCell>
                  </TableRow>
                )
              }
            })}
          </TableBody>
        </Table>
      </TableContainer>
    );
  }

  return (
    <div className={classes.containerEmpty}>
      <Typography variant="h5">No hay eventos programados</Typography>
    </div>
  )
}

export default ListSchedule