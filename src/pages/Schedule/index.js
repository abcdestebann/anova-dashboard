import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

import ListSchedule from './components/ListSchedule'
import CreateSchedule from './components/CreateSchedule';

import { getTemplates } from '../../actions/templates'
import { getUsers } from '../../actions/user'
import { getSchedules, saveSchedule, updateSchedule, deleteSchedule } from '../../actions/schedule'
import CustomDialog from '../../components/Dialog';
import CircularProgress from '@material-ui/core/CircularProgress';

class Schedule extends Component {

  state = {
    openModalCreate: false,
    templates: [],
    users: [],
    templateSelected: "",
    userSelected: "",
    title: "",
    dateSelected: new Date(),
    openDialogConfirm: false,
    scheduleToEdit: {},
    scheduleToDelete: {},
    scheduleToSee: {}
  }

  async componentDidMount() {
    await this.props.dispatch(getTemplates())
    await this.props.dispatch(getUsers())
    await this.props.dispatch(getSchedules())
  }

  toggleModelCreate = (open) => {
    this.setState({ openModalCreate: open })
    if (!open) {
      this.setState({
        templateSelected: "",
        userSelected: "",
        title: "",
        dateSelected: new Date(),
        scheduleToEdit: {},
        scheduleToSee: {}
      })
    }
  }

  handleChangeSelect = (event) => {
    const { value, name } = event.target
    this.setState({ [name]: value })
  }

  handleChageText = (event) => {
    const { value } = event.target
    this.setState({ title: value })
  }

  handleDateChange = (date) => {
    this.setState({ dateSelected: date })
  }

  handleSaveSchedule = async () => {
    const { templateSelected, userSelected, dateSelected, title, scheduleToEdit } = this.state
    const data = {
      templateId: templateSelected,
      userId: userSelected,
      date: dateSelected._d || dateSelected,
      title,
      isActived: true
    }

    if (scheduleToEdit.id) {
      data.id = scheduleToEdit.id
      await this.props.dispatch(updateSchedule(scheduleToEdit.id, data))
    } else {
      await this.props.dispatch(saveSchedule(data))
    }
    this.toggleModelCreate(false)
  }

  toggleDialogConfirm = (open, scheduleToDelete = {}) => {
    this.setState({ openDialogConfirm: open, scheduleToDelete })
  }

  handleEditSchedule = (name, schedule) => {
    this.setState({
      [name]: schedule,
      templateSelected: schedule.templateId,
      userSelected: schedule.userId,
      title: schedule.title,
      dateSelected: schedule.date.seconds ? new Date(schedule.date.seconds * 1000) : schedule.date
    })
    this.toggleModelCreate(true)
  }

  handleDeleteSchedule = () => {
    this.props.dispatch(deleteSchedule(this.state.scheduleToDelete.id))
    this.toggleDialogConfirm(false)
  }

  serializeUsers = (users) => {
    return users.map(user => ({ ...user, name: `${user.name.first} ${user.name.last}` }))
      .filter((user) => user.profile === 2 && user.state)
  }

  serializeSchedules = (schedules) => {
    return schedules.filter(schedule => schedule.isActived)
  }

  serializeTemplates = (templates) => {
    return templates.filter((template) => template.isActived)
  }

  render() {
    const { templateSelected, userSelected, title, dateSelected, openModalCreate } = this.state;
    const { templates, users, schedules, classes } = this.props;

    if (schedules.length === 0) {
      return (
        <div className={classes.containerLoading}>
          <CircularProgress color="secondary" />
        </div>
      )
    }

    return (
      <div className={classes.container}>
        <Button onClick={this.toggleModelCreate.bind(null, true)} variant="contained" color="secondary">Nueva agenda</Button>

        <ListSchedule
          schedules={this.serializeSchedules(schedules)}
          templates={templates}
          users={users}
          toggleDialogConfirm={this.toggleDialogConfirm}
          handleEditSchedule={this.handleEditSchedule}
        />

        <CreateSchedule
          open={openModalCreate}
          templates={this.serializeTemplates(templates)}
          users={this.serializeUsers(users)}
          templateSelected={templateSelected}
          userSelected={userSelected}
          title={title}
          dateSelected={dateSelected}
          scheduleToEdit={this.state.scheduleToEdit}
          scheduleToSee={this.state.scheduleToSee}
          handleChangeSelect={this.handleChangeSelect}
          handleChageText={this.handleChageText}
          handleDateChange={this.handleDateChange}
          handleClose={this.toggleModelCreate}
          handleSaveSchedule={this.handleSaveSchedule}
        />

        <CustomDialog
          open={this.state.openDialogConfirm}
          message="¿Deseas eliminar esta agenda?"
          onClickCancel={this.toggleDialogConfirm.bind(null, false)}
          onClickAccept={this.handleDeleteSchedule} />

      </div>
    )
  }
}

const styles = () => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end'
  },
  buttonLink: {
    textDecoration: "none",
  },
  containerLoading: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh'
  }
})

const mapStateToProps = ({ templatesReducer, userReducer, scheduleReducer }) => ({
  templates: templatesReducer.templates,
  users: userReducer.users,
  schedules: scheduleReducer.schedules
})

export default connect(mapStateToProps)(withStyles(styles)(Schedule))