import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import IconButton from '@material-ui/core/IconButton';
import Delete from '@material-ui/icons/Delete';
import Create from "@material-ui/icons/Create";

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

const TableTemplates = ({ templates, handleDeleteTemplate, handleGetTemplateById }) => {

  const parseDate = (seconds) => {
    const date = new Date(seconds * 1000)
    const newDate = `${date.getDate()}/${date.getMonth() + 1}/${date.getFullYear()}`
    return newDate
  }

  const classes = useStyles();

  return (
    <TableContainer style={{ marginTop: 16 }} component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Nombre</TableCell>
            <TableCell align="center">Descripción</TableCell>
            <TableCell align="center">Creado</TableCell>
            <TableCell align="center">Opciones</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {templates.map((row) => {
            if (row.isActived) {
              return (
                <TableRow key={row.id}>
                  <TableCell style={{ width: '20%' }}>{row.title}</TableCell>
                  <TableCell style={{ width: '30%' }} align="center" size="medium">{row.description || "Sin descripción"}</TableCell>
                  <TableCell align="center">{parseDate(row.date_created.seconds)}</TableCell>
                  <TableCell align="center">
                    <IconButton onClick={handleGetTemplateById.bind(null, row.id)}>
                      <Create color="primary" />
                    </IconButton>
                    <IconButton onClick={handleDeleteTemplate.bind(null, true, row)}>
                      <Delete color="error" />
                    </IconButton>
                  </TableCell>
                </TableRow>
              )
            }
          })}
        </TableBody>
      </Table>
    </TableContainer>
  );
}

export default TableTemplates