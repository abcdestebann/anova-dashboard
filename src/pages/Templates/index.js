import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles';
import { NavLink } from 'react-router-dom';

import TableTemplates from './components/TableTemplates'
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import CircularProgress from '@material-ui/core/CircularProgress';

import { getTemplates, getTemplateById, deleteTemplate } from '../../actions/templates'

class Templates extends Component {

  state = {
    openDialogConfirm: false,
    templateToDelete: {}
  }

  componentDidMount = () => {
    this.getTemplates()
  }

  getTemplates = async () => {
    await this.props.dispatch(getTemplates())
  }

  handleDeleteTemplate = () => {
    this.props.dispatch(deleteTemplate(this.state.templateToDelete))
    this.toggleDialogConfirm(false)
  }

  handleGetTemplateById = async (templateId) => {
    await this.props.dispatch(getTemplateById(templateId))
    this.props.history.push("/create-new-template")
  }

  toggleDialogConfirm = (open, templateToDelete = {}) => {
    this.setState({ openDialogConfirm: open, templateToDelete })
  }

  render() {
    const { classes, templates } = this.props;

    if (templates.length === 0) {
      return (
        <div className={classes.containerLoading}>
          <CircularProgress color="secondary" />
        </div>
      )
    }

    return (
      <div className={classes.container}>
        <NavLink to="/create-new-template" className={classes.buttonLink} activeClassName="active" key="Create">
          <Button
            variant="contained"
            color="secondary">
            Nueva plantilla
          </Button>
        </NavLink>

        <TableTemplates
          templates={templates}
          handleGetTemplateById={this.handleGetTemplateById}
          handleDeleteTemplate={this.toggleDialogConfirm} />

        <Dialog
          open={this.state.openDialogConfirm}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              ¿Deseas eliminar esta plantilla?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.toggleDialogConfirm.bind(null, false)} color="primary"> Cancelar </Button>
            <Button onClick={this.handleDeleteTemplate} color="secondary"> Aceptar </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}

const styles = () => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end'
  },
  buttonLink: {
    textDecoration: "none",
  },
  containerLoading: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh'
  }
})

const mapStateToProps = ({ templatesReducer }) => ({
  templates: templatesReducer.templates
})

export default connect(mapStateToProps)(withStyles(styles)(Templates))