import React, { Component } from 'react'
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';
import FormLabel from '@material-ui/core/FormLabel';
import Typography from '@material-ui/core/Typography';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import Done from "@material-ui/icons/Done";
import CircularProgress from '@material-ui/core/CircularProgress';
import IconButton from '@material-ui/core/IconButton';
import ArrowBack from '@material-ui/icons/ArrowBack';

import { createUser, updateUser, setUserToEdit } from '../../../actions/user'
import { isEmpty } from 'lodash'
import CustomDialog from '../../../components/Dialog';

const EMAIL_REGEX = /^([\w_\.\-\+])+\@([\w\-]+\.)+([\w]{2,10})+$/;

class CreateUser extends Component {

  state = {
    title: 'Crear usuario',
    name: '',
    lastname: '',
    email: '',
    job: '',
    phone: null,
    type: null,
    isValidEmail: true,
    buttonDisabled: true,
    openDialogSuccess: false,
    textContentDialog: 'Usuario creado satisfactoriamente',
    showLoading: false,
    emailAlreadyUsed: false
  }

  componentDidMount() {
    const { userToEdit } = this.props;
    if (!isEmpty(this.props.userToEdit)) {
      this.setState({
        title: 'Editar usuario',
        textContentDialog: 'Usuario actualizado satisfactoriamente',
        name: userToEdit.name.first,
        lastname: userToEdit.name.last,
        email: userToEdit.email,
        job: userToEdit.job,
        phone: userToEdit.phone,
        type: userToEdit.profile.toString()
      })
    }
  }

  async componentWillUnmount() {
    this.props.dispatch(setUserToEdit({}))
  }

  onChangeText = (event) => {
    const { name, value } = event.target

    this.setState({ [name]: value }, () => this.toggleButton())

    if (name === 'email') {
      this.setState({ isValidEmail: value ? EMAIL_REGEX.test(value) : true })
    }

  }

  toggleButton = () => {
    const { name, lastname, email, job, phone, type, isValidEmail } = this.state;

    if (name && lastname && email && job && phone && type && isValidEmail) {
      this.setState({ buttonDisabled: false })
    } else {
      this.setState({ buttonDisabled: true })
    }
  }

  handleClickButton = () => {
    const { name, lastname, email, job, phone, type } = this.state;

    const data = {
      email,
      job,
      name: {
        first: name,
        last: lastname
      },
      phone,
      profile: parseInt(type),
      state: true
    }
    if (isEmpty(this.props.userToEdit)) {
      this.handleCreateUser(data)
    } else {
      this.handleUpdateUser(data)
    }
  }

  handleCreateUser = async (data) => {
    try {
      this.toggleLoadingState(true)
      const date = new Date()
      data.date_created = `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()}`
      await this.props.dispatch(createUser(data))
      this.setState({ openDialogSuccess: true })
      this.toggleLoadingState(false)
    } catch (error) {
      if (error.toString().includes("email-already-in-use")) {
        this.setState({ emailAlreadyUsed: true, openDialogSuccess: true, textContentDialog: 'El correo electrónico ya existe, vuelve a intentar con uno diferente' })
      }
      this.toggleLoadingState(false)
    }
  }

  handleUpdateUser = async (data) => {
    this.toggleLoadingState(true)
    data.id = this.props.userToEdit.id
    await this.props.dispatch(updateUser(data))
    this.setState({ openDialogSuccess: true })
    this.toggleLoadingState(true)
  }

  handleButtonDialog = () => {
    this.setState({ openDialogSuccess: false, emailAlreadyUsed: false, textContentDialog: 'Usuario creado satisfactoriamente' })
    if (!this.state.emailAlreadyUsed) {
      this.props.history.goBack();
    }
  }

  toggleLoadingState = (showLoading) => {
    this.setState({ showLoading })
  }

  handleGoBack = () => {
    this.props.history.goBack()
  }

  render() {
    const { name, lastname, email, job, phone, type, buttonDisabled, openDialogSuccess, isValidEmail, emailAlreadyUsed } = this.state;
    const { classes } = this.props;

    return (
      <div className={classes.container}>
        <IconButton
          color="inherit"
          className={classes.arrowBack}
          onClick={this.handleGoBack}>
          <ArrowBack />
        </IconButton>
        <Typography variant="h4" style={{ marginTop: 60 }}>{this.state.title}</Typography>
        <div className={classes.containerInputs}>
          <TextField
            id="name"
            variant="outlined"
            className={classes.input}
            name="name"
            value={name}
            onChange={this.onChangeText}
            label="Nombre" />
          <TextField
            id="lastname"
            className={classes.input}
            name="lastname"
            variant="outlined"
            value={lastname}
            onChange={this.onChangeText}
            label="Apellido" />
        </div>
        <div className={classes.containerInputs}>
          <TextField
            id="email"
            className={classes.input}
            name="email"
            disabled={!isEmpty(this.props.userToEdit)}
            value={email}
            onChange={this.onChangeText}
            type="email"
            variant="outlined"
            error={!isValidEmail && email}
            helperText={isValidEmail ? "" : "Ingresa un correo electrónico válido."}
            label="Correo electrónico" />
          <TextField
            id="phone"
            className={classes.input}
            name="phone"
            variant="outlined"
            value={phone}
            onChange={this.onChangeText}
            type="number"
            label="Teléfono" />
        </div>
        <div className={classes.containerInputs}>
          <TextField
            id="job"
            className={classes.input}
            name="job"
            variant="outlined"
            value={job}
            onChange={this.onChangeText}
            label="Cargo" />

          <FormControl component="fieldset" className={classes.input}>
            <FormLabel component="legend">Tipo de usuario</FormLabel>
            <RadioGroup
              aria-label="type"
              name="type"
              style={{ flexDirection: 'row' }}
              value={type}
              onChange={this.onChangeText}>
              <FormControlLabel value="1" control={<Radio />} label="Administrador" />
              <FormControlLabel value="2" control={<Radio />} label="Inspector" />
            </RadioGroup>
          </FormControl>

        </div>
        <div className={classes.containerButton}>
          {
            this.state.showLoading
              ? <CircularProgress color="secondary" />
              : <Button
                style={{ width: '30%' }}
                disabled={buttonDisabled}
                onClick={this.handleClickButton}
                color="secondary"
                variant="contained">
                Guardar
               </Button>
          }
        </div>

        <CustomDialog
          open={openDialogSuccess}
          message={this.state.textContentDialog}
          onClickAccept={this.handleButtonDialog}>
          {!this.state.emailAlreadyUsed &&
            <div className={classes.containerDialog}>
              <Done className={classes.iconDone} />
            </div>}
        </CustomDialog>

      </div>
    )
  }
}

const styles = () => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    width: '100%',
    position: 'relative'
  },
  containerInputs: {
    display: 'flex',
    marginTop: 16
  },
  input: {
    flex: 1,
    margin: '0 32px 24px 0'
  },
  containerButton: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 32
  },
  containerDialog: {
    justifyContent: 'center',
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'column'
  },
  iconDone: {
    fontSize: '6em',
    color: '#8BC34A'
  },
  arrowBack: {
    position: 'absolute'
  }
})

const mapStateToProps = ({ userReducer }) => ({
  userToEdit: userReducer.userToEdit
})

export default connect(mapStateToProps)(withStyles(styles)(CreateUser))
