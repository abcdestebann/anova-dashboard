import React from 'react'
import { makeStyles } from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Create from "@material-ui/icons/Create";
import IconButton from '@material-ui/core/IconButton';
import Delete from '@material-ui/icons/Delete';

const useStyles = makeStyles({
  table: {
    minWidth: 650,
  },
});

const ListUsers = ({ users, handleEditUser, handleDeleteUser }) => {
  const classes = useStyles();

  return (
    <TableContainer style={{ marginTop: 16 }} component={Paper}>
      <Table className={classes.table} aria-label="simple table">
        <TableHead>
          <TableRow>
            <TableCell>Nombre</TableCell>
            <TableCell align="center">Correo electrónico</TableCell>
            <TableCell align="center">Cargo</TableCell>
            <TableCell align="center">Teléfono</TableCell>
            <TableCell align="center">Perfil</TableCell>
            <TableCell align="center">Opciones</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {users.map((user) => {
            if (user.state) {
              return (
                <TableRow key={user.id}>
                  <TableCell component="th" scope="row">{user.name.first} {user.name.last}</TableCell>
                  <TableCell align="center">{user.email}</TableCell>
                  <TableCell align="center">{user.job}</TableCell>
                  <TableCell align="center">{user.phone}</TableCell>
                  <TableCell align="center">{user.profile === 1 ? 'Administrador' : 'Inspector'}</TableCell>
                  <TableCell align="center">
                    <IconButton onClick={handleEditUser.bind(null, user)}>
                      <Create color="primary" />
                    </IconButton>
                    <IconButton onClick={handleDeleteUser.bind(null, true, user)}>
                      <Delete color="error" />
                    </IconButton>
                  </TableCell>
                </TableRow>
              )
            }
            return null
          })}
        </TableBody>
      </Table>
    </TableContainer>
  )
}


export default ListUsers