import React, { Component } from 'react'
import { connect } from 'react-redux'
import { withStyles } from '@material-ui/core/styles';
import { NavLink } from 'react-router-dom';
import Button from '@material-ui/core/Button';
import ListUsers from './components/ListUsers';
import Delete from '@material-ui/icons/Delete';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import CircularProgress from '@material-ui/core/CircularProgress';

import { getUsers, deleteUser, setUserToEdit } from '../../actions/user'


class Users extends Component {

  state = {
    openDialogConfirm: false,
    userSelected: {}
  }

  componentDidMount = () => {
    this.getUsers()
  }

  getUsers = async () => {
    await this.props.dispatch(getUsers())
  }

  handleEditUser = (user) => {
    this.props.dispatch(setUserToEdit(user))
    this.props.history.push("/create-user")
  }

  handleDeleteUser = () => {
    this.props.dispatch(deleteUser(this.state.userSelected))
    this.toggleDialogConfirm(false)
  }

  toggleDialogConfirm = (open, userSelected = {}) => {
    this.setState({ openDialogConfirm: open, userSelected })
  }

  render() {
    const { classes, users, user } = this.props;
    const { openDialogConfirm } = this.state;

    if (users.length === 0) {
      return (
        <div className={classes.containerLoading}>
          <CircularProgress color="secondary" />
        </div>
      )
    }

    return (
      <div className={classes.container}>
        <NavLink to="/create-user" className={classes.buttonLink} activeClassName="active" key="Create">
          <Button
            variant="contained"
            color="secondary">
            Nuevo Usuario
          </Button>
        </NavLink>

        <ListUsers
          users={users.filter(item => item.id !== user.id)}
          handleEditUser={this.handleEditUser}
          handleDeleteUser={this.toggleDialogConfirm} />

        <Dialog
          open={openDialogConfirm}
          aria-labelledby="alert-dialog-title"
          aria-describedby="alert-dialog-description">
          <DialogContent>
            <DialogContentText id="alert-dialog-description">
              ¿Deseas eliminar este usuario?
            </DialogContentText>
          </DialogContent>
          <DialogActions>
            <Button onClick={this.toggleDialogConfirm.bind(null, false)} color="primary"> Cancelar </Button>
            <Button onClick={this.handleDeleteUser} color="secondary"> Aceptar </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }
}


const styles = () => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-end'
  },
  buttonLink: {
    textDecoration: "none",
  },
  containerLoading: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    height: '100vh'
  }
})

const mapStateToProps = ({ userReducer }) => ({
  users: userReducer.users,
  user: userReducer.user
})

export default connect(mapStateToProps)(withStyles(styles)(Users))