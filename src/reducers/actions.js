import { SET_DATA_LIST_ACTIONS } from '../actions/actiontypes'

const initialState = {
  actions: []
}

const actionsReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_DATA_LIST_ACTIONS:
      return { ...state, actions: action.payload };
    default:
      return state
  }
};

export default actionsReducer