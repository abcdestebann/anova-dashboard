import { SET_DATA_LIST_SCHEDULES } from '../actions/actiontypes'

const initialState = {
  schedules: []
}

const scheduleReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_DATA_LIST_SCHEDULES:
      return { ...state, schedules: action.payload };
    default:
      return state
  }
};

export default scheduleReducer