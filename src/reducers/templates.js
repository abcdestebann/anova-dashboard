import { SET_DATA_LIST_TEMPLATES, SET_DATA_TEMPLATE_TO_EDIT } from '../actions/actiontypes'

const initialState = {
  templates: [],
  templateToEdit: {}
}

const templatesReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_DATA_LIST_TEMPLATES:
      return { ...state, templates: action.payload };
    case SET_DATA_TEMPLATE_TO_EDIT:
      return { ...state, templateToEdit: action.payload };
    default:
      return state
  }
};

export default templatesReducer
