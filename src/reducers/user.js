import { SET_DATA_USER, SET_DATA_LIST_USERS, SET_USER_TO_EDIT } from '../actions/actiontypes'

const initialState = {
  user: {},
  users: [],
  userToEdit: {}
}

const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case SET_DATA_USER:
      return { ...state, user: action.payload };
    case SET_DATA_LIST_USERS:
      return { ...state, users: action.payload };
    case SET_USER_TO_EDIT:
      return { ...state, userToEdit: action.payload };
    default:
      return state
  }
};

export default userReducer