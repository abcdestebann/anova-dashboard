// ICONS
import Dashboard from "@material-ui/icons/Dashboard";
import PhotoAlbum from "@material-ui/icons/PhotoAlbum";
import SupervisorAccount from "@material-ui/icons/SupervisorAccount";
import DateRange from "@material-ui/icons/DateRange";
import { faPaste, faUsers, faTasks, faCalendarAlt } from '@fortawesome/free-solid-svg-icons'

// PAGESfa
import Actions from '../pages/Actions';
import Templates from '../pages/Templates';
import Users from "../pages/Users";
import CreateUser from "../pages/Users/components/CreateUser";
import Schedule from "../pages/Schedule";

const DASHBOAR_ROUTES = [
   {
      path: "/templates",
      sidebarName: "Plantillas",
      navbarName: "Plantillas",
      users: [1, 2],
      icon: faPaste,
      component: Templates
   },
   {
      path: "/schedule",
      sidebarName: "Agenda",
      navbarName: "Agenda",
      users: [1, 2],
      icon: faCalendarAlt,
      component: Schedule
   },
   {
      path: "/actions",
      sidebarName: "Acciones",
      navbarName: "Acciones",
      users: [1, 2],
      icon: faTasks,
      component: Actions
   },
   {
      path: "/users",
      sidebarName: "Usuarios",
      navbarName: "Usuarios",
      users: [1],
      icon: faUsers,
      component: Users
   },
   {
      path: "/create-user",
      component: CreateUser
   },
   //{ redirect: true, path: "/", to: "/templates", navbarName: "Redirect" }
];

export default DASHBOAR_ROUTES;