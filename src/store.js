import { createStore, applyMiddleware, combineReducers } from 'redux';
import thunk from 'redux-thunk';

import userReducer from './reducers/user'
import templatesReducer from './reducers/templates'
import scheduleReducer from './reducers/schedule'
import actionsReducer from './reducers/actions'

const reducers = combineReducers({ userReducer, templatesReducer, scheduleReducer, actionsReducer })

const middlewares = applyMiddleware(thunk);

const store = createStore(
	reducers,
	undefined,
	middlewares
);

export default store;